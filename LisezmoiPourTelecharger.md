Pour charger le fichier oxt qui est une extension de LibreOffice vous n'avez qu'à cliquer sur le fichier, une fenêtre vous préviendra du téléchargement. Acceptez et c'est chargé en 3 secondes.

Attention, si vous faites " clone " vous chargerez tous les fichiers ainsi que ceux contenus dans /Files. Cela intéresse les programmeurs. A vous de voir.
